﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deneme.RestfulService.Models;
using Deneme.RestfulService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Deneme.RestfulService.Controllers
{
    [Route("v1")]
    [ApiController]
    public class DenemeController : ControllerBase
    {
        private readonly IDataServices _services;
        public DenemeController(IDataServices services)
        {
            _services = services;
        }

        [HttpPost]
        [Route("AddData")]
        public ActionResult<ItemModel> AddData(ItemModel items)
        {
            var setItems = _services.AddData(items);
            if (setItems == null)
            {
                return NotFound();
            }
            return setItems;
        }

        [HttpGet]
        [Route("GetData")]
        public ActionResult<Dictionary<string,ItemModel>> GetData()
        {
            var datas = _services.GetData();
            if (datas.Count == 0)
            {
                return NotFound();
            }

            return datas;
        }
    }
}