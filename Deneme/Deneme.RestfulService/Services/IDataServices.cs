﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deneme.RestfulService.Models;

namespace Deneme.RestfulService.Services
{
    public interface IDataServices
    {
        ItemModel AddData(ItemModel items);
        Dictionary<string, ItemModel> GetData();
    }
}
