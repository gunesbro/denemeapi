﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deneme.RestfulService.Models;

namespace Deneme.RestfulService.Services
{
    public class DataServices : IDataServices
    {
        private readonly Dictionary<string, ItemModel> _dataDict;

        public DataServices()
        {
            _dataDict = new Dictionary<string, ItemModel>();
        }

        public ItemModel AddData(ItemModel items)
        {
            if (!(_dataDict.Keys.Contains(items.ItemName)))
            {
                _dataDict.Add(items.ItemName, items);
                return items;
            }
            return null;
        }

        public Dictionary<string, ItemModel> GetData()
        {
            return _dataDict;
        }
    }
}
