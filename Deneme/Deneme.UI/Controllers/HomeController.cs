﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Deneme.UI.Models;
using RestSharp;
using Newtonsoft.Json;

namespace Deneme.UI.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var client = new RestClient("https://localhost:44344");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("/v1/AddData", Method.POST);
            //request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
            //request.AddUrlSegment("id", "123"); // replaces matching token in request.Resource

            // easily add HTTP Headers
            //request.AddHeader("header", "value");

            var modell = new Modell()
            {
                Id = 1112,
                ItemName = "Display Ports",
                Price = 156.565
            };
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(modell);
            ViewBag.sendThis = "";
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            
            ViewBag.result = response.IsSuccessful.ToString() + response.StatusCode + response.ResponseStatus + response.ErrorMessage;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            var client = new RestClient("https://localhost:44344");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("/v1/GetData", Method.GET);
            //request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
            //request.AddUrlSegment("id", "123"); // replaces matching token in request.Resource

            // easily add HTTP Headers
            //request.AddHeader("header", "value");

            //var modell = new Modell()
            //{
            //    Id = 111,
            //    ItemName = "Display Port",
            //    Price = 156.56
            //};
            //request.RequestFormat = DataFormat.Json;
            //request.AddJsonBody(modell);
            ViewBag.sendThis = "";
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string

            List<ModellList>  l =(JsonConvert.DeserializeObject<List<ModellList>>(content));
           

            ViewBag.result = l[0].list[0].Id;


            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
